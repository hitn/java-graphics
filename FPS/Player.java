import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.*;

public class Player extends JPanel{
    int x=250;
    int y=500;
    int speed = 4;
    int directionX = 0;
    int directionY = 0;
    Image image;

    Player(){
        try {
            image=ImageIO.read(new File("./ship_1.png"));
        } catch (Exception e) {
            System.out.println("Exception occured");
        }
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.setColor(Color.WHITE);
        g.drawImage(image,x, y, 50, 50,this);
    }

    public void playerUpdate(KeyEvent e){
        int code = e.getKeyCode();
        if(code == 37){       //left
            directionX = -1;
        }
        else if (code == 39){  //right
            directionX = 1;
        }
        else if (code == 38){   //up
            directionY = -1;
        }
        else if (code == 40){    //down
            directionY = 1;
        }
    }

    public void move(){
        int nextX = x + directionX * speed;
        int nextY = y + directionY * speed;

            if (nextX >= 0 && nextX <= 530) {
                x = nextX;
            }
            if (nextY >= 0 && nextY <= 500) {
                y = nextY;
            }
    
    }

    public void stop(){
        directionX = 0;
        directionY = 0;
    }
}
