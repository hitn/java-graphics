import java.awt.*;
import javax.swing.*;

public class Gamepanel extends JPanel implements Runnable{
    int x=0;
    box b=new box();
    Thread thread;

    Gamepanel(){
        super();
        this.setPreferredSize(new Dimension(500,500));
        this.setBackground(Color.black);
    }
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        b.paintComponent(g);
    }

    public void startGame(){
        thread=new Thread(this);
        thread.start();
    }
    public void update(){
        b.update();
    }
    @Override
    public void run() {
        double drawInterval=1000000000/60;
        double deltaTime=0;
        long timePassed=System.nanoTime();
        long currentTime=0;
        while(thread!=null){
            currentTime=System.nanoTime();
            deltaTime+=(currentTime-timePassed)/drawInterval;
            timePassed=currentTime;

            if(deltaTime>=1){
                update();
                repaint();
                deltaTime--;
            }
        }
    }
}