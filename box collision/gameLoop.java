import javax.swing.JFrame;

class gameLoop{
    public static void main(String[] args) {
       JFrame frame=new JFrame("Box Collision");
       Gamepanel game=new Gamepanel();
       frame.add(game);
       game.startGame();
       frame.setSize(600,600);
       frame.setResizable(false);
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       frame.setVisible(true);
    }
}