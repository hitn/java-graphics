
import java.util.Random;
import java.util.Scanner;

public class Main{
    public static void main(String[] args) {
        while(true){
            Scanner input=new Scanner(System.in);
            System.out.println("Select Rock Paper or Scissor");
            String select=input.nextLine().toLowerCase();
            Random random=new Random();
            int randomValue=random.nextInt(3);
            String compVal;

            if(randomValue==0){
                compVal="rock";
            }
            else if(randomValue==1){
                compVal="paper";
            }
            else{
                compVal="scissor";
            }
          
            System.out.println("Computer Selects "+ compVal);
            if(select.equals(compVal)){
                System.out.println("Draw");
            }
            else if(select.equals("paper") && compVal.equals("rock")){
                System.out.println("You Win");
            }
            else if(select.equals("paper") && compVal.equals("scissor")){
                System.out.println("You Lose");
            }
            else if(select.equals("rock") && compVal.equals("paper")){
                System.out.println("You Lose");
            }
            else if(select.equals("rock") && compVal.equals("scissor")){
                System.out.println("You Win");
            }
            else if(select.equals("scissor") && compVal.equals("rock")){
                System.out.println("You Lose");
            }
            else if(select.equals("scissor") && compVal.equals("paper")){
                System.out.println("You Win");
            }
            else{
                System.out.println("Invalid Input");
            }
        }
    }
}